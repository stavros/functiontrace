See https://functiontrace.com/ for end user instructions and demos.

Functiontrace is implemented as two components:
1. A server, written in Rust, which aggregates trace information from running
   processes
2. A python module, written in C and Python, which hooks in to the various 
   traced events.

### Building the server
```
$ cargo build
```

### Building the python module
```
$ cd py-functiontrace
$ python setup.py build
$ python setup.py bdist_wheel --dist-dir ./dist
```

### Testing
This currently requires installing the built modules globally.
```
# The test script will use the server built in target/release
$ pip install py-functiontrace/dist/*.whl
$ python integration_tests.py
```
