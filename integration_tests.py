#!/usr/bin/env python3
# Time how long it times to run each demo.  Additionally ensure that the demo
# returns a sane exit code.
import os
from pathlib import Path
import concurrent.futures
import subprocess
import tempfile
import time
import shutil
import sys

SERVER_TIMEOUT = 15

# Run the given demo script and verify that it generates an output.
def run_script(demo_script, trace_mem):
    # Create an tmp directory for the test artifacts
    tmpDir = Path(tempfile.mkdtemp(prefix="functiontrace-server-test_"))

    with open(os.path.join(tmpDir, "output.txt"), "w") as outputFile:
        start = time.time()
        os.environ["RUST_LOG"] = "info"
        script_process = subprocess.Popen(
            ["python3", "-m", "functiontrace", "--output_dir", tmpDir]
            + (["--trace-memory"] if trace_mem else [])
            + [str(demo_script.resolve())],
            cwd=tmpDir,
            stdout=outputFile,
            stderr=subprocess.STDOUT,
        )

    result = {
        "tmpDir": tmpDir,
    }
    try:
        script_process.wait(30)
        result["client_time"] = time.time() - start
    except subprocess.TimeoutExpired:
        script_process.kill()
        result["script_fail"] = "Timeout"
        return result

    if script_process.returncode:
        # exception.py exits with returncode 1 on purpose
        if "exception.py" != demo_script.name:
            result["script_fail"] = script_process.returncode
            return result

    # Arbitarily wait 15s for the server to output the profile file
    for _ in range(SERVER_TIMEOUT * 10):
        if (tmpDir / "functiontrace.latest.json").exists():
            break
        time.sleep(0.1)
    else:
        result["server_fail"] = None
        return result

    result["server_time"] = time.time() - result["client_time"] - start
    shutil.rmtree(tmpDir)
    return result


def main():
    demoDir = Path(__file__).parent / "demos"
    serverPath = Path(__file__).parent / "target" / "release" / "functiontrace-server"
    assert serverPath.exists(), serverPath
    os.environ["PATH"] = (
        str(serverPath.parent.resolve()) + os.pathsep + os.environ["PATH"]
    )

    failed = False
    test_times = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
        future_demos = {}
        for demo_script in [x for x in demoDir.iterdir() if x.is_file()]:
            future_demos[
                executor.submit(run_script, demo_script, False)
            ] = demo_script.name
            future_demos[
                executor.submit(run_script, demo_script, True)
            ] = "{} (mem traced)".format(demo_script.name)

        for future in concurrent.futures.as_completed(future_demos.keys()):
            demo = future_demos[future]
            print(f"====== Running {str(demo):<30} ======")

            try:
                result = future.result()

            except Exception as exc:
                print("FAIL generated an exception: {}".format(exc))
                continue

            test_times[demo] = (
                result.get("client_time", "N/A"),
                result.get("server_time", "N/A"),
            )
            if set(result.keys()) == set(["tmpDir", "client_time", "server_time"]):
                # Success
                continue

            # Failure cases
            failed = True
            if "script_fail" in result:
                print("FAIL returned {}".format(result["script_fail"]))
            elif "server_fail" in result:
                print(
                    "FAIL {}/functiontrace.latest.json does not exist after {} seconds after script exited".format(
                        result["tmpDir"], SERVER_TIMEOUT
                    )
                )
            print("Output saved in: {}".format(result["tmpDir"]))

    print("Results")
    print(
        "{script:<30} {client_time:<14} {server_time:<14}".format(
            script="Test", client_time="Client Time", server_time="Server Time"
        )
    )
    for script in sorted(test_times.keys()):
        test_time = test_times[script]
        print(
            "{script:<30} {client_time:>8.5} {server_time:>8.5}".format(
                script=script, client_time=test_time[0], server_time=test_time[1]
            )
        )

    sys.exit(1 if failed else 0)


if __name__ == "__main__":
    main()
