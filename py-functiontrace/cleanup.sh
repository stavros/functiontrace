#!/bin/sh

NORMALUSER=$1

rm -r /usr/lib64/python3.6/site-packages/functiontrace*
rm -r /usr/lib64/python3.6/site-packages/_functiontrace*
rm -r /usr/lib/python3.6/site-packages/__pycache__
rm -r /usr/lib/python3.6/site-packages/functiontrace*
rm -r /usr/lib/python3.6/site-packages/_functiontrace*
rm -r /home/$NORMALUSER/functiontrace/py-functiontrace/functiontrace.egg-info
rm -r /home/$NORMALUSER/functiontrace/py-functiontrace/build
rm -r /home/$NORMALUSER/functiontrace/py-functiontrace/target
rm -r /home/$NORMALUSER/functiontrace/py-functiontrace/dist
rm -r /home/$NORMALUSER/functiontrace/py-functiontrace/functiontrace.egg-info
rm -r /home/$NORMALUSER/functiontrace/py-functiontrace/__pycache__
