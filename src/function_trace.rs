use serde::Deserialize;
use std::time::Duration;

// A representation of the various trace messages the client can send us during normal tracing
// operation.
#[derive(Deserialize, Debug)]
#[serde(tag="type")]
pub enum FunctionTrace {
    RegisterThread(ThreadRegistration),
    Call {
        time: Duration,
        func_name: String,
        filename: String,
        linenumber: u32,
    },
    Return {
        time: Duration,
        func_name: String
    },
    NativeCall {
        time: Duration,
        func_name: String,
        module_name: String,
    },
    NativeReturn {
        time: Duration,
        func_name: String,
    },
    Exception {
        time: Duration,
        exception_type: String,
        exception_value: String,
        filename: String,
        linenumber: u32,
    },
    Log {
        time: Duration,
        log_type: String,
        log_value: String,
    },
    Import {
        time: Duration,
        module_name: String,
    },
    Allocation {
        time: Duration,
        details: AllocationDetails,
    }
}

#[derive(Deserialize, Debug)]
#[serde(tag="type")]
pub enum AllocationDetails {
    Alloc {
        bytes: usize,
        addr: usize,
    },
    Realloc {
        bytes: usize,
        old_addr: usize,
        new_addr: usize,
    },
    Free {
        old_addr: usize
    }
}

// This message contains information for initializing a trace.
#[derive(Deserialize, Debug)]
pub struct TraceInitialization {
    pub program_name: String,
    pub program_version: String,
    pub lang_version: String,
    pub platform: String,
    pub time: Duration,
}

// This message contains information for registering threads.
#[derive(Deserialize, Debug)]
pub struct ThreadRegistration {
    pub time: Duration,
    pub program_name: String,
    pub pid: usize,
}
