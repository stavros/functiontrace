// Various fields need to be emitted with a specific name for the Firefox Profiler to be happy.
// In order to allow us to specify these when necessar, we ignore case warnings.
#![allow(non_snake_case)]

pub mod profile_generator;
pub mod function_trace;
