#!/usr/bin/env python3
import time
import subprocess
import logging

def fib(x):
    if x == 0:
        return 0
    if x == 1:
        return 1
    a = fib(x-1)
    b = fib(x-2)
    return a + b

def a(x):
    if x == 1:
        return 1
    print("a", x)
    return 1 + b(x - 1)

def b(x):
    if x == 1:
        return 1
    print("b", x)
    return 1 + a(x - 1)

if __name__ == "__main__":
    # Verify that kwargs work for hooking
    __import__("textwrap", level=0)

    logging.critical("CODE RED")
    fib(12)
    logging.info("Running from:", subprocess.run("pwd"))
    time.sleep(0.015)  # Wait a bit so we can see that logs are streaming
    a(20)
